import './App.css';
import { useEffect, useState } from "react";
import SovAppBarTitle from './components/SovAppBarTitle';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
import SovQRCodeDrag from './components/SovQRCodeDrag';
import Env from './env/env';
import * as SSIConnections from 'ssi-lib/Connections';
import * as SSICredentialExchange from 'ssi-lib/CredentialExchange';

import { Card, CardContent, CardActions, Typography, DialogActions } from '@mui/material';
import { Button, Grid } from '@mui/material';
import { Box } from '@mui/system';

import { CircularProgress } from '@mui/material';
import { Stack } from '@mui/material';

import { Dialog, DialogContent, DialogTitle } from '@mui/material';

import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import { Chip } from '@mui/material';

import InfoIcon from '@mui/icons-material/Info';
import grad from './grad';

const env = new Env();

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

export default function App() {
  const api_url = env.REACT_APP_AGENT_API;
  const hook_url = env.REACT_APP_AGENT_HOOK;
  const [invite, setInvite] = useState('{}'); // '{"@type":"did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/connections/1.0/invitation","@id":"86c81391-9767-4ccb-ae88-bb6da112f389","recipientKeys":["J9Rm7L6UPkRJbjDDHonqf8ekZheQywYeQ1MDdHj5ZGrw"],"label":"cavendish","serviceEndpoint":"http://127.17.0.1:8200"}'
  const [inviteUrl, setInviteUrl] = useState("");
  const [connectionResults, setConnectionResults] = useState([]);
  const [credentialResults, setCredentialResults] = useState([]);
  const [credentialExchangeResults, setCredentialExchangeResults] = useState([]);

  const [jsonDialogOpen, setJsonDialogOpen] = useState(false);
  const [jsonDialogCredExId, setJsonDialogCredExId] = useState("");
  const [jsonDialogCredConnId, setJsonDialogCredConnId] = useState("");
  const [jsonDialogCredProposal, setJsonDialogCredProposal] = useState("");

  useEffect(SSIConnections.startWebSocketEffect(hook_url, setConnectionResults), []);
  useEffect(SSIConnections.fetchConnectionsEffect(api_url, setConnectionResults), []);

  useEffect(() => {
    createNewInvite();
  }, []);

  async function createNewInvite() {
    let local_invite = await SSIConnections.createInvitation(api_url, "public", true, true);

    setInvite(local_invite.invitation);
    setInviteUrl(local_invite.invitation_url);
  }

  useEffect(SSICredentialExchange.startWebSocketEffect(hook_url, setCredentialExchangeResults), []);

  // original fetchData() is not yet implemented (this would just get any pending credential applications)

  function sendOffer(cred_ex_id) {
    SSICredentialExchange.sendCredentialOffer(api_url, cred_ex_id);
  }

  const Credential = (props) => {
    let attributes = props.details.cred_preview.attributes;

    // we need to check for null pointers, e.g. [0] may not exist:
    const legalName = attributes.filter((x) => x.name === 'legalName')[0].value;
    const userName = attributes.filter((x) => x.name === 'userName')[0].value;
    const employer = attributes.filter((x) => x.name === 'employer')[0].value;
    const instagramHandle = attributes.filter((x) => x.name === 'instagramHandle')[0].value;

    var state = props.details.state;
    if (!state) state = "Invalid"; // completed ?

    const cred_ex_id = props.details.cred_ex_id;

    const proposal_received = state === "proposal-received";
    const offer_sent = state === "offer-sent";
    const request_received = state === "request-received";
    const credential_issued = state === "credential-issued";
    const done = state === "done";

    return (
      <Box m={3} borderRadius={4}
        sx={{
          width: 420,
          height: 180,
          background: grad(137, 189, 238),
          '&:hover': {
            backgroundColor: 'white',
            opacity: [1.0, 1.0, 1.0],
          },
        }}
      >
        <Box m={1}>
          <Stack direction="row">
            <Stack>
              <Box m={1}>
                <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                  Credential Application
                </Typography>
              </Box>
              <Box m={1}>
                <Typography sx={{ mb: 0.5 }} color="text.secondary">{legalName}</Typography>
              </Box>
            </Stack>
            <Box ml={5} mt={1}>
              <Typography variant="body2">
                Username: {userName} <br />
                Employer: {employer} <br />
                Instagram: {instagramHandle}
              </Typography>
            </Box>
          </Stack>
          <CardActions>
            {proposal_received && <Chip style={{ borderColor: 'transparent' }} variant="outlined" icon={<InfoIcon style={{ color: 'green' }} />} label="New application"></Chip>}
            {proposal_received && <Button size="small" onClick={() => sendOffer(cred_ex_id)}>Send Offer</Button>}
            {offer_sent && <Stack direction="row"><CircularProgress /><Typography m={1}>Awaiting applicant response.</Typography></Stack>}
            {request_received && <Chip style={{ borderColor: 'transparent' }} variant="outlined" icon={<CheckCircleIcon style={{ color: 'green' }} />} label="Offer accepted"></Chip>}
            {request_received && <Button size="small" onClick={() => SSICredentialExchange.issueCredentials(api_url, cred_ex_id)}>Send Credential</Button>}
            {proposal_received && <Button size="small" onClick={() => {
              setJsonDialogCredExId(props.details.cred_ex_id);
              setJsonDialogCredConnId(props.details.connection_id);
              setJsonDialogCredProposal(JSON.stringify(props.details));
              setJsonDialogOpen(true)
            }}
            >
              View JSON
            </Button>}
            {credential_issued && <Chip style={{ borderColor: 'transparent' }} variant="outlined" icon={<CheckCircleIcon style={{ color: 'green' }} />} label="Credential issued successfully"></Chip>}
            {done && <Chip style={{ borderColor: 'transparent' }} variant="outlined" icon={<CheckCircleIcon style={{ color: 'green' }} />} label="Credential issued successfully"></Chip>}
          </CardActions>
        </Box>
      </Box>);
  }

  const JsonDialog = () => {
    return <Dialog open={jsonDialogOpen}>
      <DialogTitle>JSON Details</DialogTitle>
      <DialogContent>
        <Typography style={{ fontWeight: 600 }}>Credential Exchange Id</Typography>
        <p>{jsonDialogCredExId}</p>
        <Typography style={{ fontWeight: 600 }}>Connection Id</Typography>
        <p>{jsonDialogCredConnId}</p>
        <Typography style={{ fontWeight: 600 }}>Credential Proposal</Typography>
        <p>{jsonDialogCredProposal}</p>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => { setJsonDialogOpen(false) }}>Close</Button>
      </DialogActions>
    </Dialog>
  }

  return (
    <Box>
      <SovAppBarTitle title={'Cavendish Credentials'} />
      <Box m={3}>
        <Grid container>
          <Grid item xs={7} style={{ marginTop: 100 }}><Typography variant="h4">Public Facing:</Typography></Grid>
          <Grid item xs={5} style={{ marginTop: 100 }}><Typography variant="h4">Private Portal:</Typography></Grid>
          <Grid item xs={7}>
            <SovQRCodeDrag
              directions="Drag and drop this invite into your wallet to start certification !"
              data={invite}>
            </SovQRCodeDrag>
            <Button onClick={() => createNewInvite()}>Generate A New Invite</Button>
          </Grid>
          <Grid item xs={5} >
            <Typography variant="h5">Applications</Typography>
            <Typography> Credential applications will appear here..
            </Typography>
            {credentialExchangeResults.map((x, index) => <Credential key={index} details={x} />)}
          </Grid>
        </Grid>
        {/* <SovBottomNav /> */}
        <JsonDialog></JsonDialog>
      </Box >
    </Box >
  );
}
