import React, { useCallback } from 'react'
import { useDropzone } from 'react-dropzone'

export default function SovDropZoneAndRead(props) {
  const onDrop = useCallback((acceptedFiles) => {
    let file = acceptedFiles[0];
    props.setImageFile(file);
  }, [])
  const { getRootProps, getInputProps } = useDropzone({ onDrop })

  return (
    <div {...getRootProps()}>
      <input {...getInputProps()} />
      <img
        width={500}
        src={props.dropImage}
        alt="Drop an image or click here"
        loading="lazy"
        style={{ margin: 10 }}
      />
    </div>
  )
}
