import QRCode from 'qrcode.react';

export default function QRCodeDrag(props) {

    function dragStart(e) {
        e.dataTransfer.setData("text", JSON.stringify(props.data));
        return true;
    }
    
    return (
        <div style={{ marginLeft: 15 }}>
            <h1>{props.title}</h1>
            <p>{props.directions}</p>
            <QRCode
                onDragStart={dragStart}
                draggable
                size={256}
                includeMargin={true}
                value={JSON.stringify(props.data)}
                renderAs='canvas'
            />
        </div>
    );
}
