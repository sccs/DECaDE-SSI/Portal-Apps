import './App.css';
import { useEffect, useState } from "react";
import SovAppBarTitle from './components/SovAppBarTitle';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
import SovQRCodeDrag from './components/SovQRCodeDrag';
import Env from './env/env';
import * as SSIConnections from 'ssi-lib/Connections';
import * as SSICredentialExchange from 'ssi-lib/CredentialExchange';
import * as SSIPresentationsHelper from 'ssi-lib/PresentationsHelper';

import { Card, CardMedia, CardContent, Typography } from '@mui/material';
import { Button, Grid } from '@mui/material';
import { Box } from '@mui/system';

import { CircularProgress } from '@mui/material';
import { Stack } from '@mui/material';

import { Chip } from '@mui/material';

import { TextField } from '@mui/material';

import dropimage from "./assets/dropzone.jpg";

import FingerprintIcon from '@mui/icons-material/Fingerprint';
import { Tabs, Tab } from '@mui/material';

import * as SSIFileServerHelper from 'ssi-lib/FileServerHelper';

import { ImageListItem } from '@mui/material';

const universalStore = require('./env/universalObject').certifications;

const env = new Env();

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

export default function App() {
  const api_url = env.REACT_APP_AGENT_API;
  const hook_url = env.REACT_APP_AGENT_HOOK;
  const FILE_SERVER_URL = "http://localhost:5000/";

  const IMAGE_VERIFIED = 100;
  const IMAGE_NOT_VERIFIED = 200;
  const IMAGE_ERROR = 300;

  const [invite, setInvite] = useState('{}');
  const [inviteUrl, setInviteUrl] = useState("");
  const [connectionResults, setConnectionResults] = useState([]);
  const [credentialResults, setCredentialResults] = useState([]);
  const [credentialExchangeResults, setCredentialExchangeResults] = useState([]);
  const [presentationResults, setPresentationResults] = useState([]);

  const [imageHash, setImageHash] = useState(undefined); // autocomputed
  const [imageUrl, setImageUrl] = useState(undefined); // autocomputed
  const [imageFile, setImageFile] = useState(undefined);
  const [dropImage, setDropImage] = useState(dropimage);

  const [value, setValue] = useState("home");
  const [verified, setVerified] = useState(false);

  const [images, setImages] = useState([]); // image repository list

  useEffect(() => {
    if (value !== "home") return;
    setImageFile(undefined);
    setImageUrl(undefined);
    setImageHash(undefined);
    setDropImage(dropimage);
    setVerified(false);
  }, [value]);

  useEffect(SSIConnections.startWebSocketEffect(hook_url, setConnectionResults), []);
  useEffect(SSIConnections.fetchConnectionsEffect(api_url, setConnectionResults), []);

  useEffect(() => {
    if (connectionResults.length === 0) return;

    const conn = connectionResults[0]; // todo *** should search presentationResults for a match against INVITATION; don't just get the "latest" one ([0]) - see below also

    switch (conn.rfc23_state) {
      case "request-received":
        console.log("request-received");
        console.log("---->", conn.connection_id);
        SSIConnections.acceptRequest(api_url, conn.connection_id);
        break;

      case "response-sent":
        console.log("response-sent");
        console.log("---->", conn.connection_id);
        setTimeout(() => {
          SSIConnections.trustPing(api_url, conn.connection_id);
        }, 1000);
        break;

      case "completed":
        if (!invite.label) return; // no invite was found, so the web socket presentation marked as completed should not be actioned - it must have been processed earlier
        console.log("completed");

        let body = {};
        body.connection_id = conn.connection_id;
        body.comment = JSON.stringify({
          flag: "ImageAuthorshipRequest",
          imgHash: imageHash, // why do we (i.e. the original webcontroller) send the image hash ??? the holder could use this
          // to cheat as this comment appears back at Ruth. this would be ok, say, if the only way we can
          // get the fingerprint back on Franklin is via the credential, without relying on what Ruth says... 

          // i think the imageHash is sent to Ruth purely for reference purposes because Ruth only
          // sends back (presents) "credential -> referent" so presumably Aries uses this reference
          // to check the "coment" fields automatically ????

          // need to look into this more !
        });
        body.proof_request = { name: "Proof Request" };
        body.proof_request.nonce = "1"; // temporary nonce
        body.proof_request.requested_attributes = {
          additionalProp1: {
            name: "imgHash",
            restrictions: [
              {
                cred_def_id: universalStore.imageAuthorshipCertification.cred_def_id,
              },
            ],
          },
        };
        body.proof_request.requested_predicates = {};
        body.proof_request.version = "1.0";
        console.log("sendpresentationrequest --->", body);
        SSIPresentationsHelper.sendPresentationRequest(api_url, body);

        break;
    }
  }, [connectionResults]);

  useEffect(() => {
    if (presentationResults.length === 0) return;
    const conn = presentationResults[0]; // todo *** should search presentationResults for a match against INVITATION; don't just get the "latest" one ([0]) - see below also

    switch (conn.state) {
      case "presentation_received":
        console.log("presentation_received");
        console.log("---->", conn.presentation_exchange_id);
        SSIPresentationsHelper.verifyPresentation(api_url, conn.presentation_exchange_id);
        break;

      case "verified":
        console.log("verified");
        verifyHash(conn);
        setValue("done");

        break;
    }
  }, [presentationResults]);

  function verifyHash(conn) {
    if (!(conn.state == "verified" && conn.presentation_request_dict &&
      conn.presentation_request_dict.comment.includes("ImageAuthorshipRequest") &&
      conn.role == "verifier")) {

      setVerified(IMAGE_ERROR);
      return;
    }

    console.log("verify hash!", "----> presentation details = ", conn.presentation)

    const status = conn.presentation.requested_proof.revealed_attrs.additionalProp1.raw === imageHash;

    setVerified(status ? IMAGE_VERIFIED : IMAGE_NOT_VERIFIED);
  }

  async function createNewInvite() {
    let local_invite = await SSIConnections.createInvitation(api_url, "public", true, true);
    setInvite(local_invite.invitation);
    setInviteUrl(local_invite.invitation_url);
  }

  useEffect(SSICredentialExchange.startWebSocketEffect(hook_url, setCredentialExchangeResults), []);
  useEffect(SSIPresentationsHelper.startWebSocketEffect(hook_url, setPresentationResults), []);

  const Interaction = (props) => {
    var geolocation, imgHash, imgTimestamp, imgUrl, macAddress;
    var image_url;
    var presentation = false;
    var credential = false;

    // *** THE BELOW COMMENTED CODE IS JUST WHAT WAS COPIED OVER FROM A DIFFERENT PORTAL
    //     WE MAY FIND IT USEFUL AS REFERENCE WHEN UPDATING THIS PORTAL'S VIEW-CREDENTIAL
    //     CREDIT-CARD COMPONENT LATER - THEN JUST DELETE IT...

    // var flag;
    // var presentation_received;
    // var proposal_received, offer_sent, request_received, credential_issued, done, issue_credential;
    // var attributes;

    // // let attributes = props.details.cred_preview.attributes;

    // var state = props.details.state;
    // if (!state) state = "Invalid";

    // const cred_ex_id = props.details.cred_ex_id;

    // if (props.details.presentation_proposal_dict) {  // is this a presentation ?

    //   attributes = JSON.parse(props.details.presentation_proposal_dict.comment);

    //   flag = attributes.flag;
    //   geolocation = attributes.geolocation;
    //   imgHash = attributes.imgHash;
    //   imgTimestamp = attributes.imgTimestamp;
    //   imgUrl = attributes.imgUrl;
    //   macAddress = attributes.macAddress;

    //   image_url = FILE_SERVER_URL + "uploads/" + imgUrl;

    //   // presentation states
    //   proposal_received = state === "proposal_received"; // "_" NOT "-" here --> is this internal to ACA-Py ??? not sure...
    //   presentation_received = state === "presentation_received"; // as above
    //   verified = state === "verified";

    //   console.log("presentation", props.details);

    //   presentation = true;
    // }
    // else { // is this a ... what other states are viewed on the thoday side ?
    //   proposal_received = state === "proposal-received";
    //   offer_sent = state === "offer-sent";
    //   request_received = state === "request-received";
    //   credential_issued = state === "credential-issued";
    //   done = state === "done";

    //   console.log("other", props.details);

    //   credential = true;
    // }

    return (
      <Box m={3}>
        <Card style={{ background: 'lightblue' }}>
          {image_url &&
            <CardMedia
              component="img"
              height="140"
              image={image_url}
              alt="unknown image"
            />}
          <CardContent>
            <Typography sx={{ fontSize: 24 }} color="text.secondary" gutterBottom>
              {presentation ? "Presentation" : (credential ? "Credential Request" : "Unknown")}
            </Typography>
            <Typography variant="body2">
              Geolocation: {geolocation} <br />
              Image Hash: {imgHash} <br />
              Timestamp: {imgTimestamp} <br />
              URL: {imgUrl} <br />
              Mac Address: {macAddress} <br />
            </Typography>
          </CardContent>
        </Card >
      </Box>);
  }

  return (
    <Box>
      <SovAppBarTitle
        title={"Franklin News"}
      />
      <Box m={3}>
        <Grid container>
          <Grid item xs={7} style={{ marginTop: 100 }}><Typography variant="h4">Public Facing:</Typography></Grid>
          <Grid item xs={5} style={{ marginTop: 100 }}><Typography variant="h4">Private Portal:</Typography></Grid>
          <Grid item xs={7}>
            <Tabs
              value={value}
              textColor="primary"
              indicatorColor="primary"
              aria-label="secondary tabs example"
            >
              <Tab value="home" label="Home" />
              <Tab value="details" label="Photo Details" />
              <Tab value="invite" label="Invitation" />
              <Tab value="done" label="All Done" />
            </Tabs>
            {value === "home" && renderHomeTab()}
            {value === "details" && renderDetailsTab()}
            {value === "invite" && renderInviteTab()}
            {value === "done" && renderDoneTab()}
          </Grid>
          <Grid item xs={5} >
            <Typography variant="h5">Applications</Typography>
            <Typography> Credential applications will appear here..
            </Typography>
            {[...presentationResults, ...credentialExchangeResults]
              .sort((x, y) => new Date(x.updated_at) - new Date(y.updated_at))
              .map((x, index) => <Interaction key={index} details={x} />)}
          </Grid>
        </Grid>
      </Box >
    </Box >
  );

  function renderHomeTab() {
    return <div>
      <Typography ml={1} mt={3} variant="h5" >Welcome to Franklin News</Typography>
      <br></br>
      <Typography ml={1}>Using this service you can submit</Typography>
      <Typography mt={3} ml={4}>• a photographer credential proving membership</Typography>
      <Typography ml={4}>• a photograph credential proving ownership</Typography>
      <Typography ml={1} mt={3}>for publication in Franklin News.</Typography>
      <Box align="center" m="auto">
        <Button
          style={{ marginTop: 40, backgroundColor: "blue" }}
          variant="contained"
          onClick={async () => {
            setValue("details");
            createNewInvite(); // create a new invite before switching tabs
            setImages(await SSIFileServerHelper.getImagesFromRegistry(FILE_SERVER_URL)); // update the image list
          }}>
          CONTINUE
        </Button>
      </Box>
    </div>;
  }

  function renderDetailsTab() {
    return <div>
      <Typography mt={3} style={{ fontWeight: "bold" }}>Select your photograph</Typography>
      <Paper style={{ overflow: 'auto' }} elevation={0}>
        <Stack direction="row">
          {images.map((image, index) => (
            <ImageListItem key={"image" + index}>
              <Box m={1}
                sx={{ width: 200 }}>
                <img
                  src={FILE_SERVER_URL + "imageRegistry/" + image}
                  width="100%"
                  onClick={async () => {
                    let hash = (await SSIFileServerHelper.imageHash(FILE_SERVER_URL, image)).data.message;
                    setImageHash(hash);
                    setImageFile(image);
                  }}
                />
              </Box>
            </ImageListItem>
          ))}
        </Stack>
      </Paper>
      <Typography mt={3} style={{ fontWeight: "bold" }}>Enter photograph details</Typography>
      <Grid container>
        <Grid xs={3}>
          <Box mt={4}>
            {imageFile && <Paper style={{ overflow: 'auto', maxWidth: "100%" }} elevation={0}>
              <img src={FILE_SERVER_URL + "imageRegistry/" + imageFile} width="100%" />
            </Paper>}
          </Box>
        </Grid>
        <Grid xs={9}>
          <Box
            component="form"
            sx={{
              '& > :not(style)': { m: 1, width: '95%' },
            }}
            noValidate
            autoComplete="off"
            m={1}
          >
            <TextField id="standard-basic" label="Name" variant="standard" />
            <TextField id="standard-basic" label="Description" variant="standard" />
          </Box>
        </Grid>
        <Grid xs={12}>
          {
            imageHash &&
            <Chip style={{ margin: 10, width: 500, backgroundColor: 'transparent' }} icon={<FingerprintIcon fontSize="large" />} label={imageHash} >
            </Chip>
          }
        </Grid>
      </Grid>
      <Box align="center" m="auto">
        <Button
          style={{ marginTop: 40, backgroundColor: "blue" }}
          variant="contained"
          onClick={() => setValue("invite")}>
          CONTINUE
        </Button>
      </Box>
    </div>;
  }

  function renderInviteTab() {
    return <div>
      <SovQRCodeDrag
        directions="Drag and drop this invite into your wallet to start photo verification."
        data={invite}>
      </SovQRCodeDrag>
      <Stack mt={4} direction="row">
        {!verified &&
          <Stack direction="row">
            <CircularProgress />
            <Typography ml={2} mt={1}>Awaiting presentation</Typography>
          </Stack>}
        {verified && <Typography ml={2} mt={1}>Processing...</Typography>}
      </Stack>
    </div>;
  }

  function renderDoneTab() {
    return <div>
      {verified === IMAGE_VERIFIED && <Typography mt={3}>Your photograph was successfully verified !</Typography>}
      {verified === IMAGE_NOT_VERIFIED && <Typography mt={3}>The presented image hash does not match the hash of the image you selected in Franklin News.</Typography>}
      {verified === IMAGE_ERROR && <Typography mt={3}>Something went wrong with the verification process, please try again.</Typography>}
      <Box align="center" m="auto">
        <Button
          style={{ marginTop: 40, backgroundColor: "blue" }}
          variant="contained"
          onClick={() => setValue("home")}>
          HOME
        </Button>
      </Box>
    </div>;
  }
}
