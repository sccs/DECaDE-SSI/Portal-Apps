/*

        this class performs the same tasks as the original webcontroller startup.js
        but specific to the agent in question. however, the setup code itself is
        the same for all agents, so this has now been put inside ssi-lib/StartupHelper.js

        turns out ENDORSER is a special authority agent with write access to the
        blockchain, so endorser is required by an agent to write to the blockchain

        the various .env variables are included as class members, but in production on
        a server or mobile device, these should go in .env ideally

    *** WARNING ***

        turns out the universalObject produced by this class, is also ACCESSED by the
        Holder-App, so make sure that which is produced from running the agent is also
        present in the holder app's /env/universalObject.json file

        once generated, this data seems to persist, thankfully. we will need to discuss
        how to deal with this in a production release at some later point !

*/

import * as StartupHelper from "ssi-lib/StartupHelper";

export default class Env {

    // ----------- these would normally come from .env variables -------------

    REACT_APP_AGENT_API = 'http://localhost:8321/'
    REACT_APP_AGENT_HOOK = 'ws://localhost:8320/socket/'

    // Franklin doesn't have AGENT_URL or AGENT_PUBLIC_DID

    REACT_APP_ENDORSER_API = 'http://localhost:8021/'       // the endorser agent has permission to access the blockchain
    REACT_APP_ENDORSER_HOOK = 'ws://localhost:8020/socket/' // (does this make it a steward ?)

    // -----------------------------------------------------------------------

    endorserURL = this.REACT_APP_ENDORSER_API;
    agentURL = this.REACT_APP_AGENT_API;

    endorserSocketURL = this.REACT_APP_ENDORSER_HOOK;
    agentSocketURL = this.REACT_APP_AGENT_HOOK;

    // trustAnchor = {
    //     "did": "V4SGRU86Z58d6TV7PBUe6f",
    //     "verkey": "GJ1SzoWzavQYfNL9XkaJdrQejfztN4XqdsiV4ct3LXKL",
    //     "posture": "posted",
    //     "key_type": "ed25519",
    //     "method": "sov"
    // };

    // schema = {
    //     "schema_name": "ImageAuthorship",
    //     "schema_version": "1.0",
    //     "attributes": [
    //         "imgUrl",
    //         "imgHash",
    //         "imgTimestamp",
    //         "geolocation",
    //         "macAddress",
    //     ],
    // };

    // endorserConnectionId;

    //

    // universalObject = {};

    constructor() {
        this.setup(); // async
    }

    async setup() {

        // there doesn't appear to be any setup for Franklin in startup.js ...



        // console.log('Running startup config...');

        // let ws = new WebSocket(this.endorserSocketURL + 'topic/endorse_transaction');
        // let wsCC = new WebSocket(this.agentSocketURL + 'topic/connections')

        // ws.onopen = () => { ws.send('web controller connected to /topic/endorse_transaction/'); };
        // wsCC.onopen = () => { wsCC.send('web controller connected to /topic/connections/') }

        // await StartupHelper.configureAgents(this.endorserURL, this.trustAnchor);
        // await StartupHelper.displayDetails(this.endorserURL, this.agentURL);
        // await StartupHelper.configureConnections();
        // this.endorserConnectionId = await StartupHelper.configureEndorser(this.agentURL, this.endorserURL);
        // await StartupHelper.configureSchemas(this.agentURL, this.schema, this.endorserConnectionId, this.endorserURL, ws);
        // await StartupHelper.configureCredDefs(this.agentURL, this.schema, this.endorserConnectionId, this.endorserUrl, ws);
        // this.universalObject = await StartupHelper.configureGlobalStore(this.agentURL, this.schema);

        // ws.close();
        // wsCC.close();
    }
}
