import './App.css';
import { useEffect, useState } from "react";
import SovBottomNav from './components/SovBottomNav';
import SovAppBarTitle from './components/SovAppBarTitle';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
import SovQRCodeDrag from './components/SovQRCodeDrag';
import Env from './env/env';
import * as SSIConnections from 'ssi-lib/Connections';
import * as SSICredentialExchange from 'ssi-lib/CredentialExchange';
import * as SSIPresentationsHelper from 'ssi-lib/PresentationsHelper';
import * as SSIFileServerHelper from 'ssi-lib/FileServerHelper';

import { Card, CardMedia, CardContent, CardActions, Typography, DialogActions } from '@mui/material';
import { Button, Grid } from '@mui/material';
import { Box } from '@mui/system';

import { CircularProgress } from '@mui/material';
import { Stack } from '@mui/material';

import { Dialog, DialogContent, DialogTitle } from '@mui/material';

import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import { Chip } from '@mui/material';

import InfoIcon from '@mui/icons-material/Info';
import grad from './grad';

const env = new Env();

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

export default function App() {
  const api_url = env.REACT_APP_AGENT_API;
  const hook_url = env.REACT_APP_AGENT_HOOK;
  const FILE_SERVER_URL = "http://localhost:5000/";

  const [invite, setInvite] = useState('{}'); // '{"@type":"did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/connections/1.0/invitation","@id":"86c81391-9767-4ccb-ae88-bb6da112f389","recipientKeys":["J9Rm7L6UPkRJbjDDHonqf8ekZheQywYeQ1MDdHj5ZGrw"],"label":"cavendish","serviceEndpoint":"http://127.17.0.1:8200"}'
  const [inviteUrl, setInviteUrl] = useState("");
  const [connectionResults, setConnectionResults] = useState([]);
  const [credentialResults, setCredentialResults] = useState([]);
  const [credentialExchangeResults, setCredentialExchangeResults] = useState([]);
  const [presentationResults, setPresentationResults] = useState([]);

  const [jsonDialogOpen, setJsonDialogOpen] = useState(false);
  const [jsonDialogCredExId, setJsonDialogCredExId] = useState("");
  const [jsonDialogCredConnId, setJsonDialogCredConnId] = useState("");
  const [jsonDialogCredProposal, setJsonDialogCredProposal] = useState("");

  useEffect(SSIConnections.startWebSocketEffect(hook_url, setConnectionResults), []);
  useEffect(SSIConnections.fetchConnectionsEffect(api_url, setConnectionResults), []);

  useEffect(() => {
    createNewInvite();
  }, []);

  async function createNewInvite() {
    let local_invite = await SSIConnections.createInvitation(api_url, "public", true, true);
    setInvite(local_invite.invitation);
    setInviteUrl(local_invite.invitation_url);
  }

  useEffect(SSICredentialExchange.startWebSocketEffect(hook_url, setCredentialExchangeResults), []);
  useEffect(SSIPresentationsHelper.startWebSocketEffect(hook_url, setPresentationResults), []);

  async function issueCredential(connection_id, info) {

    let image = await SSIFileServerHelper.moveToRegistry(FILE_SERVER_URL, info.imgUrl);

    let body = {};
    body.connection_id = connection_id;
    body.credential_preview = {};
    body.credential_preview.type = "issue-credential/2.0/credential-preview";
    body.credential_preview.attributes = [
      {
        name: "imgHash",
        value: info.imgHash,
      },
      {
        name: "imgUrl",
        value: FILE_SERVER_URL + "imageRegistry/" + info.imgUrl
      },
      {
        name: "imgTimestamp",
        value: info.imgTimestamp,
      },
      {
        name: "macAddress",
        value: info.macAddress,
      },
      {
        name: "geolocation",
        value: info.geolocation,
      },
    ];
    body.filter = {};
    body.filter.indy = env.universalObject.certification;

    const proposalResponse = await SSICredentialExchange.sendCredentialOfferDirect(api_url, body);
  }

  const Credential = (props) => {
    console.log(props.details);

    var flag, geolocation, imgHash, imgTimestamp, imgUrl, macAddress;
    var presentation_received, verified;
    var proposal_received, offer_sent, request_received, credential_issued, done, issue_credential;
    var image_url;
    var presentation = false;
    var credential = false;

    var attributes;

    // let attributes = props.details.cred_preview.attributes;

    var state = props.details.state;
    if (!state) state = "Invalid";

    const cred_ex_id = props.details.cred_ex_id;
    console.log("details", props.details);

    if (props.details.presentation_proposal_dict) {  // is this a presentation ?

      attributes = JSON.parse(props.details.presentation_proposal_dict.comment);

      flag = attributes.flag;
      geolocation = attributes.geolocation;
      imgHash = attributes.imgHash;
      imgTimestamp = attributes.imgTimestamp;
      imgUrl = attributes.imgUrl;
      macAddress = attributes.macAddress;

      image_url = FILE_SERVER_URL + "uploads/" + imgUrl;

      // presentation states
      proposal_received = state === "proposal_received"; // "_" NOT "-" here >>> is this internal to ACA-Py ??? not sure...
      presentation_received = state === "presentation_received"; // as above
      verified = state === "verified";
      console.log("state", state);

      presentation = true;
    }
    else { // is this a ... what other states are viewed on the thoday side ?
      proposal_received = state === "proposal-received";
      offer_sent = state === "offer-sent";
      request_received = state === "request-received";
      credential_issued = state === "credential-issued";
      done = state === "done";

      credential = true;
    }

    return (
      <Box m={3} borderRadius={4}
        sx={{
          width: 420,
          height: 400,
          background: grad(137, 189, 238),
          '&:hover': {
            backgroundColor: 'white',
            opacity: [1.0, 1.0, 1.0],
          },
        }}
      >
        <Box m={1}>
          <Stack mb={3}>
            <Box mt={1}>
              {image_url &&
                <CardMedia mt={2}
                  component="img"
                  height="140"
                  image={image_url}
                  alt="unknown image"
                  style={{
                    borderRadius: '3%',
                  }}
                />}
            </Box>
            <Typography mb={2} sx={{ fontSize: 24 }} color="text.secondary" gutterBottom>
              {presentation ? "Presentation" : (credential ? "Credential Request" : "Unknown")}
            </Typography>
            <Typography variant="body2">
              Geolocation: {geolocation} <br />
            </Typography>
            <Typography variant="body2">
              Image Hash: {imgHash?.substring(0, 30)}... <br />
            </Typography>
            <Typography variant="body2">
              Timestamp: {imgTimestamp} <br />
            </Typography>
            <Typography variant="body2">
              URL: {imgUrl} <br />
            </Typography>
            <Typography variant="body2">
              Mac Address: {macAddress} <br />
            </Typography>
          </Stack>
          <CardActions>
            {presentation && proposal_received && <Chip style={{ borderColor: 'transparent' }} variant="outlined" icon={<InfoIcon style={{ color: 'green' }} />} label="New Presentation"></Chip>}
            {presentation && proposal_received && <Button size="small" onClick={
              () => {
                console.log("1--->", api_url, props.details.presentation_exchange_id);
                console.log("should be similar to: 1--->  http://localhost:8121/ 2be4c7a9-a0df-4d85-87f1-dd7820403de4");
                SSIPresentationsHelper.sendPresentationRequest(api_url, {}, props.details.presentation_exchange_id)
              }}>
              Send Request
            </Button>}

            {presentation && presentation_received && <Chip style={{ borderColor: 'transparent' }} variant="outlined" icon={<InfoIcon style={{ color: 'green' }} />} label="Presentation received"></Chip>}
            {presentation && presentation_received && <Button size="small" onClick={() =>
              SSIPresentationsHelper.verifyPresentation(api_url, props.details.presentation_exchange_id)}
            >
              Verify
            </Button>}

            {presentation && verified && <Chip style={{ borderColor: 'transparent' }} variant="outlined" icon={<InfoIcon style={{ color: 'green' }} />} label="Verified !"></Chip>}
            {presentation && verified && <Button size="small" onClick={() => issueCredential(props.details.connection_id, attributes)}>
              Issue Credential
            </Button>}

            {offer_sent && <Stack direction="row"><CircularProgress /><Typography m={1}>Awaiting applicant response.</Typography></Stack>}
            {request_received && <Chip style={{ borderColor: 'transparent' }} variant="outlined" icon={<CheckCircleIcon style={{ color: 'green' }} />} label="Offer accepted"></Chip>}
            {request_received && <Button size="small" onClick={() => SSICredentialExchange.issueCredentials(api_url, cred_ex_id)}>Send Credential</Button>}
            {proposal_received && <Button size="small" onClick={() => {
              setJsonDialogCredExId(props.details.cred_ex_id);
              setJsonDialogCredConnId(props.details.connection_id);
              setJsonDialogCredProposal(JSON.stringify(props.details));
              setJsonDialogOpen(true)
            }}
            >
              View JSON
            </Button>}
            {credential_issued && <Chip style={{ borderColor: 'transparent' }} variant="outlined" icon={<CheckCircleIcon style={{ color: 'green' }} />} label="Credential issued successfully"></Chip>}
            {done && <Chip style={{ borderColor: 'transparent' }} variant="outlined" icon={<CheckCircleIcon style={{ color: 'green' }} />} label="Credential issued successfully"></Chip>}
          </CardActions>
        </Box>
      </Box>);
  }

  const JsonDialog = () => {
    return <Dialog open={jsonDialogOpen}>
      <DialogTitle>JSON Details</DialogTitle>
      <DialogContent>
        <Typography style={{ fontWeight: 600 }}>Credential Exchange Id</Typography>
        <p>{jsonDialogCredExId}</p>
        <Typography style={{ fontWeight: 600 }}>Connection Id</Typography>
        <p>{jsonDialogCredConnId}</p>
        <Typography style={{ fontWeight: 600 }}>Credential Proposal</Typography>
        <p>{jsonDialogCredProposal}</p>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => { setJsonDialogOpen(false) }}>Close</Button>
      </DialogActions>
    </Dialog>
  }

  return (
    <Box>
      <SovAppBarTitle
        title={"Thoday's Photo Credential Service"}
      />
      <Box m={3}>
        <Grid container>
          <Grid item xs={7} style={{ marginTop: 100 }}><Typography variant="h4">Public Facing:</Typography></Grid>
          <Grid item xs={5} style={{ marginTop: 100 }}><Typography variant="h4">Private Portal:</Typography></Grid>
          <Grid item xs={7}>
            <SovQRCodeDrag
              directions="Drag and drop this invite into your wallet to start certification !"
              data={invite}>
            </SovQRCodeDrag>
            <Button onClick={() => createNewInvite()}>Generate A New Invite</Button>
          </Grid>
          <Grid item xs={5} >
            <Typography variant="h5">Applications</Typography>
            <Typography> Credential applications will appear here..
            </Typography>
            {[...presentationResults, ...credentialExchangeResults]
              .sort((x, y) => new Date(x.updated_at) - new Date(y.updated_at))
              .map((x, index) => <Credential key={index} details={x} />)}
          </Grid>
        </Grid>
        <SovBottomNav />
        <JsonDialog></JsonDialog>
      </Box >
    </Box >
  );
}
